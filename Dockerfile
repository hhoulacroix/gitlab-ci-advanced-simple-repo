FROM eclipse-temurin:22 as jre-build

ARG JAR_FILE

WORKDIR /app
COPY target/${JAR_FILE} /app/app.jar

RUN $JAVA_HOME/bin/jlink \
         --add-modules $(jdeps --ignore-missing-deps --print-module-deps -q /app/app.jar),java.base,java.xml,jdk.unsupported,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument \
         --strip-debug \
         --no-man-pages \
         --no-header-files \
         --compress=2 \
         --output /javaruntime

FROM debian:buster-slim
ENV JAVA_HOME=/opt/java/openjdk
ENV PATH "${JAVA_HOME}/bin:${PATH}"
COPY --from=jre-build /javaruntime $JAVA_HOME

RUN apt-get update && apt-get install -y curl jq

# Continue with your application deployment
RUN mkdir /opt/app
COPY --from=jre-build /app/app.jar /opt/app/app.jar

HEALTHCHECK --interval=10s --timeout=15s --start-period=30s \
  CMD curl --silent --fail http://localhost:8080/rest/actuator/health | jq --exit-status '.status == "UP"' || exit 1

EXPOSE 8080/tcp

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=prod,actuator","-jar","/opt/app/app.jar"]
